<?php
/**
 * Created by PhpStorm.
 * User: Biten Das
 * Date: 5/24/2021
 * Time: 12:56 AM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Material Design for Bootstrap</title>

    <!--Favicon -->
    <link rel="icon" href="../img/logo.png" type="image/x-icon" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" />
    <!-- Google Fonts Roboto -->
    <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"
    />


    <!-- MDB -->
    <link rel="stylesheet" href="../css/mdb.min.css" />
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
<header>
    <!--this for top menu-->
    <div class="top-menu">

        <div class="container d-flex justify-content-between">
            <div class="location"> 1234, Park Street, New York, America</div>

            <div class="welcome bg-dark"> Welcome to PRODUSTRY </div>

            <div class="social-icon"> 1234, Park Street, New York, America</div>

        </div>

    </div>

    <!--this for main menu-->
    <div id="main-menu">
        <div class="container d-flex justify-content-between">


            <!--this is logo-->
            <div class="logo"><a href="index.html"> <img src="img/logo.png" alt="logo"></a> </div>

            <!--this for Navigation-->

            <nav class="">

                <ul class="nav">
                    <li class="nav-item"><a class="nav-link active-color" href="#"> Home </a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> Services </a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> About Us  </a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> Portfolio</a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> Our Blog</a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> Contact Us</a></li>
                </ul>

            </nav>

        </div>
    </div>

</header>

