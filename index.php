
<?php
//include ("inc/header.php");
?>
<?php
/**
 * Created by PhpStorm.
 * User: Biten Das
 * Date: 5/24/2021
 * Time: 12:56 AM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Material Design for Bootstrap</title>

    <!--Favicon -->
    <link rel="icon" href="img/logo.png" type="image/x-icon" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" />
    <!-- Google Fonts Roboto -->
    <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap"
    />


    <!-- MDB -->
    <link rel="stylesheet" href="css/mdb.min.css" />
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<header>
    <!--this for top menu-->
    <div class="top-menu">

        <div class="container d-flex justify-content-between">
            <div class="location"> 1234, Park Street, New York, America</div>

            <div class="welcome bg-dark"> Welcome to PRODUSTRY </div>

            <div class="social-icon"> 1234, Park Street, New York, America</div>

        </div>

    </div>

    <!--this for main menu-->
    <div id="main-menu">
        <div class="container d-flex justify-content-between">


            <!--this is logo-->
            <div class="logo"><a href="index.php"> <img src="img/logo.png" alt="logo"></a> </div>

            <!--this for Navigation-->

            <nav class="">

                <ul class="nav">
                    <li class="nav-item"><a class="nav-link active-color" href="#"> Home </a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> Services </a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> About Us  </a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> Portfolio</a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> Our Blog</a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> Contact Us</a></li>
                </ul>

            </nav>

        </div>
    </div>

</header>


<!-- Carousel wrapper -->
    <div
            id="carouselBasicExample"
            class="carousel slide carousel-fade"
            data-mdb-ride="carousel"
    >
      <!-- Indicators -->
      <div class="carousel-indicators">
        <button
                type="button"
                data-mdb-target="#carouselBasicExample"
                data-mdb-slide-to="0"
                class="active"
                aria-current="true"
                aria-label="Slide 1"
        ></button>
        <button
                type="button"
                data-mdb-target="#carouselBasicExample"
                data-mdb-slide-to="1"
                aria-label="Slide 2"
        ></button>
        <button
                type="button"
                data-mdb-target="#carouselBasicExample"
                data-mdb-slide-to="2"
                aria-label="Slide 3"
        ></button>
      </div>

      <!-- Inner -->
      <div class="carousel-inner">
        <!-- Single item -->
        <div class="carousel-item active">
          <img
                  src="img/banner.jpg"
                  class="d-block w-100"
                  alt="Banner Image"
          />

          <div class="carousel-caption d-none d-md-block">

            <h5> We Make Your <span class="active-color"> Dream </span> </h5>
            <p>We are the best construction factory on production based ever. You can get every type ofort in the construction related work from us. Just feel free and come to us. We will help you to get the best solution.</p>

              <button class="btn  carousel-btn">VIEW SOLUTION </button>
              <button class="btn carousel-btn">PURCHASE NOW </button>
          </div>


        </div>

        <!-- Single item -->
        <div class="carousel-item ">
          <img
                  src="img/banner2.jpg"
                  class="d-block w-100"
                  alt="..."
          />
          <div class="carousel-caption slider-caption d-none d-md-block">
            <h5>The road to success is<span class="active-color"> Always </span>  under construction</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

              <button class="btn carousel-btn">VIEW SOLUTION </button>
              <button class="btn carousel-btn">PURCHASE NOW </button>
          </div>
        </div>

        <!-- Single item -->
        <div class="carousel-item">
          <img
                  src="img/banner4.jpg"
                  class="d-block w-100"
                  alt="..."
          />
          <div class="carousel-caption d-none d-md-block">
            <h5>They missed a great opportunity to shut  up</h5>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>

              <button class="btn carousel-btn">VIEW SOLUTION </button>
              <button class="btn carousel-btn">PURCHASE NOW </button>
          </div>
        </div>
      </div>
      <!-- Inner -->

      <!-- Controls -->
      <button
              class="carousel-control-prev"
              type="button"
              data-mdb-target="#carouselBasicExample"
              data-mdb-slide="prev"
      >
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button
              class="carousel-control-next"
              type="button"
              data-mdb-target="#carouselBasicExample"
              data-mdb-slide="next"
      >
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
    <!-- Carousel wrapper -->

    <!-- Start your project here-->
   <section id="service">
     <div class="container">

       <div class="text-center section-head-content">
         <h3> Our Services </h3>
         <p>We provide you a better service. you have no complain about our service, it’s guaranteed.</p>
       </div>

       <div class="row">
         <div class="col-md-4">
           <div class="icon-border"> <img src="img/service-icon-1.png" alt="service-icon"> </div>

           <div class="card-body">
             <h4> Building Design </h4>
             <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's. </p>
           </div>
         </div>

         <div class="col-md-4">
           <div class="icon-border"> <img src="img/service-icon-2.png" alt="service-icon"> </div>

           <div class="card-body">
             <h4> Up lifting </h4>
             <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's. </p>
           </div>
         </div>
         <div class="col-md-4">
           <div class="icon-border"> <img src="img/service-icon-3.png" alt="service-icon"> </div>

           <div class="card-body">
             <h4> Hard Laborer </h4>
             <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's. </p>
           </div>
         </div>



       </div>


     </div>
   </section>


    <section id="about">

        <div class="row">

            <div class="col-md-6">
                <img src="img/about-image.png" alt="About Image">
            </div>

            <div class="col-md-6">



                    <h3>About US</h3>

                    <!-- Tabs navs -->
                    <ul class="nav nav-tabs nav-fill mb-3" id="ex1" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a
                                    class="nav-link active"
                                    id="company"
                                    data-mdb-toggle="tab"
                                    href="#company-url"
                                    role="tab"
                                    aria-controls="company-url"
                                    aria-selected="true"
                            >Company</a
                            >
                        </li>
                        <li class="nav-item" role="presentation">
                            <a
                                    class="nav-link"
                                    id="history"
                                    data-mdb-toggle="tab"
                                    href="#history-url"
                                    role="tab"
                                    aria-controls="history-url"
                                    aria-selected="false"
                            > History </a
                            >
                        </li>
                        <li class="nav-item" role="presentation">
                            <a
                                    class="nav-link"
                                    id="mission"
                                    data-mdb-toggle="tab"
                                    href="#mission-url"
                                    role="tab"
                                    aria-controls="mission-url"
                                    aria-selected="false"
                            >Mission</a
                            >
                        </li>
                        <li class="nav-item" role="presentation">
                            <a
                                    class="nav-link"
                                    id="awards"
                                    data-mdb-toggle="tab"
                                    href="#awards-url"
                                    role="tab"
                                    aria-controls="awards-url"
                                    aria-selected="false"
                            >Awards</a
                            >
                        </li>


                    </ul>
                    <!-- Tabs navs -->

                    <!-- Tabs content -->
                    <div class="tab-content" id="ex2-content">
                        <div
                                class="tab-pane fade show active"
                                id="company-url"
                                role="tabpanel"
                                aria-labelledby="company"
                        >
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aspernatur assumenda distinctio, eligendi enim est fuga fugiat, hic ipsa iste laborum necessitatibus nobis porro quos repellendus reprehenderit repudiandae similique vitae.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aspernatur assumenda distinctio, eligendi enim est fuga fugiat,</p>
                        </div>
                        <div
                                class="tab-pane fade"
                                id="history-url"
                                role="tabpanel"
                                aria-labelledby="history"
                        >
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aspernatur assumenda distinctio, eligendi enim est fuga fugiat, necessitatibus nobis porro quos repellendus reprehenderit repudiandae similique vitae.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aspernatur assumenda distinctio, eligendi enim est fuga fugiat,</p>
                        </div>

                        <div
                                class="tab-pane fade"
                                id="mission-url"
                                role="tabpanel"
                                aria-labelledby="mission"
                        >

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aspernatur assumenda distinctio, eligendi enim est fuga fugiat, hic ipsa iste laborum necessitatibus nobis porro quos repellendus  similique vitae.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aspernatur assumenda distinctio, eligendi enim est fuga ,</p>

                        </div>

                         <div
                                class="tab-pane fade"
                                id="awards-url"
                                role="tabpanel"
                                aria-labelledby="awards"
                        >
                             <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aspernatur assumenda distinctio, eligendi enim est fuga fugiat, hic  laborum necessitatibus nobis porro quos repellendus reprehenderit repudiandae similique vitae.</p>
                             <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium  assumenda distinctio, eligendi enim est fuga fugiat,</p>
                        </div>

                        <button class="btn about-btn"> REad MORE </button>



                    </div>
                    <!-- Tabs content -->



            </div>

        </div>



    </section>

    <!-- End your project here-->







    <!-- MDB -->
    <script type="text/javascript" src="js/mdb.min.js">


    </script>
    <!-- Custom scripts -->
    <script type="text/javascript"></script>
  </body>
</html>
